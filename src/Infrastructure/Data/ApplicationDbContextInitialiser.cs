﻿using System.Runtime.InteropServices;
using JobApplication.Domain.Entities;
using JobApplication.Domain.Enums;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace JobApplication.Infrastructure.Data;

public static class InitialiserExtensions
{
    public static async Task InitialiseDatabaseAsync(this WebApplication app)
    {
        using var scope = app.Services.CreateScope();

        var initialiser = scope.ServiceProvider.GetRequiredService<ApplicationDbContextInitialiser>();

        await initialiser.InitialiseAsync();

        await initialiser.SeedAsync();
    }
}

public class ApplicationDbContextInitialiser(
    ILogger<ApplicationDbContextInitialiser> logger,
    ApplicationDbContext context)
{
    public async Task InitialiseAsync()
    {
        try
        {
            await context.Database.MigrateAsync();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "An error occurred while initialising the database.");
            throw;
        }
    }

    public async Task SeedAsync()
    {
        try
        {
            await TrySeedAsync();
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "An error occurred while seeding the database.");
            throw;
        }
    }
    
    public async Task TrySeedAsync()
    {
        // Default data
        // Seed, if necessary
        if (!context.JobApplicationResults.Any())
        {
            context.JobApplicationResults.Add(new JobApplicationResults
            {
                CompanyId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                JobId = Guid.Parse("35b5f96b-640f-41f0-bda2-a6130d4bdf33"),
                CandidateId = Guid.Parse("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"),
                Title = "Job Application Result",
                Description = "Job Application Result Description",
                Result = JobApplicationResult.InterviewPlanned
            });
            context.JobApplicationResults.Add(new JobApplicationResults
            {
                CompanyId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                JobId = Guid.Parse("192bb5fa-62af-4b65-a481-faead454a623"),
                CandidateId = Guid.Parse("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"),
                Title = "Job Application Result",
                Description = "Job Application Result Description",
                Result = JobApplicationResult.WaitingForResults
            });
            context.JobApplicationResults.Add(new JobApplicationResults
            {
                CompanyId = Guid.Parse("87850b4b-1244-41b7-b038-6ec8181914bc"),
                JobId = Guid.Parse("02c42469-2b57-42d7-a40b-76bcefd80796"),
                CandidateId = Guid.Parse("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"),
                Title = "Job Application Result",
                Description = "Job Application Result Description",
                Result = JobApplicationResult.Denied
            });

            await context.SaveChangesAsync();
        }

        if (!context.Interviews.Any())
        {
            context.Interviews.Add(new Interview
            {
                InterviewerId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                JobId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                CandidateId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                Date = DateTime.Now,
                Title = "Interview",
                Address = "Interview Address",
                Type = InterviewType.Online
            });
            await context.SaveChangesAsync();
        }
    }
}
