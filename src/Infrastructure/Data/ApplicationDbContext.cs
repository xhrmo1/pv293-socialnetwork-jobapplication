﻿using System.Reflection;
using JobApplication.Application.Common.Interfaces;
using JobApplication.Domain.Entities;
using JobApplication.Infrastructure.Data.Migrations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JobApplication.Infrastructure.Data;

public class ApplicationDbContext(DbContextOptions options) : DbContext(options), IApplicationDbContext
{
    public DbSet<JobApplicationResults> JobApplicationResults => Set<JobApplicationResults>();
    public DbSet<Interview> Interviews => Set<Interview>();
    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        builder.SeedEntities();
        base.OnModelCreating(builder);
    }
}
