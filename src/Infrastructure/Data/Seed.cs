﻿using JobApplication.Domain.Entities;
using JobApplication.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace JobApplication.Infrastructure.Data.Migrations;

public static class Seed
{
    public static void SeedEntities(this ModelBuilder builder)
    {
        var jobApplicationResults = PrepairJobApplicationResults();
        var interviews = PrepairInterviews();
        builder.Entity<JobApplicationResults>().HasData(jobApplicationResults);
        builder.Entity<Interview>().HasData(interviews);
    }

    private static List<JobApplicationResults> PrepairJobApplicationResults()
    {
        return new List<JobApplicationResults>()
        {
            new JobApplicationResults
            {
                Id = 1,
                CompanyId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                JobId = Guid.Parse("35b5f96b-640f-41f0-bda2-a6130d4bdf33"),
                CandidateId = Guid.Parse("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"),
                Title = "Job Application Result",
                Description = "Job Application Result Description",
                Result = JobApplicationResult.InterviewPlanned
            },
            new JobApplicationResults
            {
                Id = 2,
                CompanyId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                JobId = Guid.Parse("192bb5fa-62af-4b65-a481-faead454a623"),
                CandidateId = Guid.Parse("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"),
                Title = "Job Application Result",
                Description = "Job Application Result Description",
                Result = JobApplicationResult.WaitingForResults
            },
            new JobApplicationResults
            {
                Id = 3,
                CompanyId = Guid.Parse("87850b4b-1244-41b7-b038-6ec8181914bc"),
                JobId = Guid.Parse("02c42469-2b57-42d7-a40b-76bcefd80796"),
                CandidateId = Guid.Parse("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"),
                Title = "Job Application Result",
                Description = "Job Application Result Description",
                Result = JobApplicationResult.Denied
            }
        };
    }

    private static List<Interview> PrepairInterviews()
    {
        return new List<Interview>
        {
            new Interview
            {
                Id = 1,
                InterviewerId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                JobId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                CandidateId = Guid.Parse("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"),
                Date = DateTime.UtcNow,
                Title = "Interview",
                Address = "Interview Address",
                Type = InterviewType.Online
            }
        };
    }
}
