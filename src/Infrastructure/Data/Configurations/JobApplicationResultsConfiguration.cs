﻿using JobApplication.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace JobApplication.Infrastructure.Data.Configurations;

public class JobApplicationResultsConfiguration : IEntityTypeConfiguration<JobApplicationResults>
{
    public void Configure(EntityTypeBuilder<JobApplicationResults> builder)
    {
        builder.Property(t => t.Title)
            .HasMaxLength(100)
            .IsRequired();
        builder.Property(t => t.Description)
            .HasMaxLength(2000)
            .IsRequired();
    }
}
