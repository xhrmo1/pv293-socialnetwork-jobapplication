﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace JobApplication.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Interviews",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    InterviewerId = table.Column<Guid>(type: "uuid", nullable: false),
                    JobId = table.Column<Guid>(type: "uuid", nullable: false),
                    CandidateId = table.Column<Guid>(type: "uuid", nullable: false),
                    Date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Title = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Address = table.Column<string>(type: "text", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Interviews", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobApplicationResults",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CompanyId = table.Column<Guid>(type: "uuid", nullable: false),
                    JobId = table.Column<Guid>(type: "uuid", nullable: false),
                    CandidateId = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    Description = table.Column<string>(type: "character varying(2000)", maxLength: 2000, nullable: false),
                    Result = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobApplicationResults", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Interviews",
                columns: new[] { "Id", "Address", "CandidateId", "Date", "InterviewerId", "JobId", "Title", "Type" },
                values: new object[] { 1, "Interview Address", new Guid("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"), new DateTime(2024, 1, 18, 22, 48, 57, 503, DateTimeKind.Utc).AddTicks(2726), new Guid("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"), new Guid("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"), "Interview", 1 });

            migrationBuilder.InsertData(
                table: "JobApplicationResults",
                columns: new[] { "Id", "CandidateId", "CompanyId", "Description", "JobId", "Result", "Title" },
                values: new object[,]
                {
                    { 1, new Guid("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"), new Guid("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"), "Job Application Result Description", new Guid("35b5f96b-640f-41f0-bda2-a6130d4bdf33"), 2, "Job Application Result" },
                    { 2, new Guid("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"), new Guid("b3f7d8c2-8a4b-4f3c-9b1e-6a0f1e9a1f1a"), "Job Application Result Description", new Guid("192bb5fa-62af-4b65-a481-faead454a623"), 0, "Job Application Result" },
                    { 3, new Guid("8bde7aa7-3dcd-4f75-abc6-48f54c27b2a8"), new Guid("87850b4b-1244-41b7-b038-6ec8181914bc"), "Job Application Result Description", new Guid("02c42469-2b57-42d7-a40b-76bcefd80796"), 1, "Job Application Result" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Interviews");

            migrationBuilder.DropTable(
                name: "JobApplicationResults");
        }
    }
}
