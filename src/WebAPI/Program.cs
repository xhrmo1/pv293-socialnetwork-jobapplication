using JobApplication.Infrastructure.Data;
var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options => {} );
builder.Services.AddInfrastructureServices(builder.Configuration);
builder.Services.AddDbContext<ApplicationDbContext>();
builder.Services.AddApplicationServices();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(options => {} );
app.UseHttpsRedirection();
await app.InitialiseDatabaseAsync();


app.MapControllers();

app.Run();
