﻿using ErrorOr;
using JobApplication.Application.Application.Commands;
using JobApplication.Application.Application.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication1.Endpoints;

[ApiController]
public class JobApplication() : Controller
{
    [HttpGet]
    [Route("[controller]/live")]
    public Task<IActionResult> GetLiveJobApplications()
    {
        return Task.FromResult<IActionResult>(Ok("Live Job Applications"));
    }
    
    [HttpGet]
    [Route("[controller]/all")]
    public async Task<IResult> GetAllJobApplications(ISender sender)
    {
        return Results.Ok(await sender.Send(new GetJobApplicationResults()));
    }
    
    [HttpPut]
    [Route("[controller]/accept/{id}")]
    public async Task<IResult> AcceptJobApplicationHandle(ISender sender, int id)
    {
        var res = await sender.Send(new AcceptJobApplicationCommand(id));
        return res.MatchFirst(
            Results.Ok,
            error => error.Type switch
            {
                ErrorType.NotFound => Results.NotFound(error.Description),
                _ => Results.BadRequest(error.Description)
            }
        );
    }
    
    [HttpPut]
    [Route("[controller]/deny/{id}")]
    public async Task<IResult> DenyJobApplicationHandle(ISender sender,int id)
    {
        var res = await sender.Send(new DenyJobApplicationCommand(id));
        return res.MatchFirst(
            Results.Ok,
            error => error.Type switch
            {
                ErrorType.NotFound => Results.NotFound(error.Description),
                _ => Results.BadRequest(error.Description)
            }
        );
    }

}
