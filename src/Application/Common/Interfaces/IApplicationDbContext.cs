﻿using JobApplication.Domain.Entities;

namespace JobApplication.Application.Common.Interfaces;

public interface IApplicationDbContext
{
    DbSet<JobApplicationResults> JobApplicationResults { get; }

    DbSet<Interview> Interviews { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}
