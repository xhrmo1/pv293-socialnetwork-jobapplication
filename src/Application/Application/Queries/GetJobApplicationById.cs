﻿using JobApplication.Application.Common.Interfaces;
using JobApplication.Domain.Entities;

namespace JobApplication.Application.Application.Queries;

public class GetJobApplicationById(IApplicationDbContext dbContext)
{
    public async Task<JobApplicationResults?> Handle(int id, CancellationToken cancellationToken = default)
    {
        return await dbContext.JobApplicationResults.FindAsync(new object?[] { id }, cancellationToken: cancellationToken);
    }

}
