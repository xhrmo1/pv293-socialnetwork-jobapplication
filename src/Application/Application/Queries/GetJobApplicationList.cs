﻿using JobApplication.Application.Common.Interfaces;
using JobApplication.Domain.Entities;
using JobApplication.Domain.Enums;

namespace JobApplication.Application.Application.Queries;

public class GetJobApplicationResults : IRequest<List<JobApplicationResults>>; 

public class GetJobApplicationHandler(IApplicationDbContext dbContext): IRequestHandler<GetJobApplicationResults,List<JobApplicationResults>>
{
    public Task<List<JobApplicationResults>> Handle(GetJobApplicationResults request, CancellationToken cancellationToken)
    {
        return  dbContext.JobApplicationResults.ToListAsync(cancellationToken: cancellationToken);
    }
}
