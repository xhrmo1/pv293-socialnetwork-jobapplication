﻿using ErrorOr;
using JobApplication.Application.Common.Interfaces;
using JobApplication.Domain.Enums;

namespace JobApplication.Application.Application.Commands;
public class DenyJobApplicationCommand(int jobId) : IRequest<ErrorOr<int>>
{
    public int JobId { get; set; } = jobId;
}
public class DenyJobApplicationCommandHandle(IApplicationDbContext dbContext): IRequestHandler<DenyJobApplicationCommand, ErrorOr<int>>
{
    public async  Task<ErrorOr<int>> Handle(DenyJobApplicationCommand request, CancellationToken cancellationToken)
    {
        var application = await dbContext.JobApplicationResults.FindAsync(request.JobId);
        if( application == null)
        {
            throw new NotFoundException(nameof(JobApplicationResult), request.JobId.ToString());
        }

        application.Result = JobApplicationResult.Denied;
        await dbContext.SaveChangesAsync(cancellationToken);
        return application.Id;

    }
}
