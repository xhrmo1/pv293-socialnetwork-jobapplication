﻿using System.Runtime.InteropServices.JavaScript;
using ErrorOr;
using JobApplication.Application.Common.Interfaces;
using JobApplication.Domain.Entities;
using JobApplication.Domain.Enums;

namespace JobApplication.Application.Application.Commands;

public class AcceptJobApplicationCommand(int jobId) : IRequest<ErrorOr<int>>
{
    public int JobId { get; set; } = jobId;
}

public class AcceptJobApplicationCommandHandle(IApplicationDbContext dbContext)
    : IRequestHandler<AcceptJobApplicationCommand, ErrorOr<int>>
{
    public async Task<ErrorOr<int>> Handle(AcceptJobApplicationCommand request, CancellationToken cancellationToken)
    {
        var application = await dbContext.JobApplicationResults.FindAsync(request.JobId);
        if (application == null)
        {
            throw new NotFoundException(nameof(JobApplicationResult), request.JobId.ToString());
        }
        
        application.Result = JobApplicationResult.InterviewPlanned;
        await dbContext.SaveChangesAsync(cancellationToken);
        return await PrepareInterview(application);
    }

    private async Task<int> PrepareInterview(JobApplicationResults jobApplicationResults)
    {
        var interview = new Interview
        {
            InterviewerId = jobApplicationResults.CompanyId,
            Date = DateTime.Now.AddDays(7).ToUniversalTime(),
            JobId = jobApplicationResults.JobId,
            Type = InterviewType.InPlace,
            Title = jobApplicationResults.Title,
            Address = "company address",
            CandidateId = jobApplicationResults.CandidateId
            
        };
        dbContext.Interviews.Add(interview);
        await dbContext.SaveChangesAsync();
        return interview.Id;
    }
    
}
