﻿namespace JobApplication.Domain.Enums;

public enum InterviewType
{
    InPlace = 0,
    Online = 1,
}
