﻿namespace JobApplication.Domain.Enums;

public enum JobApplicationResult
{
    WaitingForResults = 0,
    Denied = 1,
    InterviewPlanned = 2,
}
