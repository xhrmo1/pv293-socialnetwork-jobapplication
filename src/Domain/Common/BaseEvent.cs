﻿using MediatR;

namespace JobApplication.Domain.Common;

public abstract class BaseEvent : INotification
{
}
