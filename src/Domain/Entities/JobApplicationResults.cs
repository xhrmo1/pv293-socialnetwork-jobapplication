﻿namespace JobApplication.Domain.Entities;

public class JobApplicationResults : BaseEntity
{
    public Guid CompanyId { get; set; }
    public Guid JobId { get; set; }
    public Guid CandidateId { get; set; }
    public string Title { get; set; } = null!;
    public string Description { get; set; } = null!;
    public JobApplicationResult Result { get; set; }
}
