﻿namespace JobApplication.Domain.Entities;

public class Interview: BaseEntity
{
    public Guid InterviewerId { get; set; }
    public Guid JobId { get; set; }
    public Guid CandidateId { get; set; }
    public DateTime Date { get; set; }
    public string Title { get; set; } = null!;
    public string Address { get; set; } = null!;
    public InterviewType Type { get; set; }
}
