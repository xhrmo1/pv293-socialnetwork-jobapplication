﻿global using JobApplication.Domain.Common;
global using JobApplication.Domain.Entities;
global using JobApplication.Domain.Enums;
global using JobApplication.Domain.Events;
global using JobApplication.Domain.ValueObjects;
