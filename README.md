﻿# Project PV293 - Social Network 
 part of project for PV293 course at FI MUNI:
- SocialNetwork.JobApplication
- [SocialNetwork.JobOffers](https://gitlab.fi.muni.cz/xkadavy/pv293-socialnetwork-joboffers/-/tree/main/JobOffers?ref_type=heads)
- [SocialNetwork.Social](https://gitlab.fi.muni.cz/xbily/pv293-socialnetwork-social/-/tree/master/src?ref_type=heads)
- project is based on [Clean.Architecture.Solution.Template](https://github.com/jasontaylordev/CleanArchitecture)


# JobApplication

The project was generated using the [Clean.Architecture.Solution.Template](https://github.com/jasontaylordev/JobApplication) version 8.0.2.

## Build

Run `dotnet build -tl` to build the solution.

## Run

To run the web application:

```bash
cd .\src\Web\
dotnet watch run
```

Navigate to https://localhost:5001. The application will automatically reload if you change any of the source files.

## Code Styles & Formatting

The template includes [EditorConfig](https://editorconfig.org/) support to help maintain consistent coding styles for multiple developers working on the same project across various editors and IDEs. The **.editorconfig** file defines the coding styles applicable to this solution.

## Code Scaffolding

The template includes support to scaffold new commands and queries.

Start in the `.\src\Application\` folder.

Create a new command:

```
dotnet new ca-usecase --name CreateTodoList --feature-name TodoLists --usecase-type command --return-type int
```

Create a new query:

```
dotnet new ca-usecase -n GetTodos -fn TodoLists -ut query -rt TodosVm
```

Run migrations:
```
 dotnet ef database update --project src\Infrastructure\Infrastructure.csproj --startup-project src\WebApplication1\WebApplication1.csproj --context JobApplication.Infrastructure.Data.Appli
cationDbContext --configuration Debug 20240118224857_Initial --connection "Host=localhost;Port=5432;Database=job-application-db;Username=postgres;Password=example"

```

If you encounter the error *"No templates or subcommands found matching: 'ca-usecase'."*, install the template and try again:

```bash
dotnet new install Clean.Architecture.Solution.Template::8.0.2
```

## Test

The solution contains unit, integration, and functional tests.

To run the tests:
```bash
dotnet test
```

## Help
To learn more about the template go to the [project website](https://github.com/jasontaylordev/CleanArchitecture). Here you can find additional guidance, request new features, report a bug, and discuss the template with other users.